#!/bin/bash

. ./cmd.sh
[ -f path.sh ] && . ./path.sh

num_threads=8
cleanup=true

set -e

echo $* > /dev/stderr

. utils/parse_options.sh

if [ $# -ne 2 ]; then
  echo "Usage: $0 <workdir> <spk>"
  exit 1;
fi

dir=$1
spk=$2

function finish {
  rm -rf "$dir"
}

$cleanup && trap finish EXIT

mkdir -p $dir/test
/bin/ls $dir/audio/*.* | sort | awk '{printf("%04d ffmpeg -i %s  -f sox - | sox -t sox - -c 1 -b 16 -t wav - rate -v 16k |\n", NR, $1)}' > $dir/test/wav.scp 

cat $dir/test/wav.scp | awk '{print $1, "'$spk'"}' | sort > $dir/test/utt2spk
utils/utt2spk_to_spk2utt.pl $dir/test/utt2spk > $dir/test/spk2utt


steps/make_mfcc.sh --mfcc-config extractor/conf/mfcc_sid.conf --nj 1 $dir/test > /dev/stderr
steps/compute_cmvn_stats.sh $dir/test > /dev/stderr
sid/compute_vad_decision.sh --vad-config extractor/conf/vad.conf --nj 1 $dir/test > /dev/stderr

sid/extract_ivectors.sh --nj 1 --num-threads $num_threads  extractor/extractor_2048 $dir/test $dir/test_ivectors > /dev/stderr

python3 local/update_ivectors.py \
    speaker_data/name_ivector.ark speaker_data/name_num_utts.ark \
    $dir/test_ivectors/spk_ivector.ark $dir/test_ivectors/num_utts.ark \
    speaker_data/name_ivector.ark speaker_data/name_num_utts.ark
