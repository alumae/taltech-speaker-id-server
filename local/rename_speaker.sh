#!/bin/bash

. ./cmd.sh
[ -f path.sh ] && . ./path.sh

set -e

echo $* > /dev/stderr

. utils/parse_options.sh

if [ $# -ne 2 ]; then
  echo "Usage: $0  <oldspk> <newspk>"
  exit 1;
fi

oldspk=$1
newspk=$2

copy-int-vector ark:speaker_data/name_num_utts.ark ark,t:- | perl -npe '$s1="'$oldspk'"; $s2="'$newspk'"; s{^$s1 }{$s2 };' > speaker_data/name_num_utts.ark.new
mv speaker_data/name_num_utts.ark.new speaker_data/name_num_utts.ark

copy-vector ark:speaker_data/name_ivector.ark ark,t:- | perl -npe '$s1="'$oldspk'"; $s2="'$newspk'"; s{^$s1 }{$s2 };' | copy-vector ark:- ark:speaker_data/name_ivector.ark.new
mv speaker_data/name_ivector.ark.new speaker_data/name_ivector.ark