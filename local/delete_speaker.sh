#!/bin/bash

. ./cmd.sh
[ -f path.sh ] && . ./path.sh

set -e

echo $* > /dev/stderr

. utils/parse_options.sh

if [ $# -ne 1 ]; then
  echo "Usage: $0  <spk>"
  exit 1;
fi

spk=$1

copy-int-vector ark:speaker_data/name_num_utts.ark ark,t:- | grep -v "^$spk " > speaker_data/name_num_utts.ark.new
mv speaker_data/name_num_utts.ark.new speaker_data/name_num_utts.ark

copy-vector ark:speaker_data/name_ivector.ark ark,t:- | grep -v "^$spk " | copy-vector ark:- ark:speaker_data/name_ivector.ark.new
mv speaker_data/name_ivector.ark.new speaker_data/name_ivector.ark