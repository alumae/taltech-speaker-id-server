#! /usr/bin/env python3

import argparse
import kaldiio
import numpy as np

if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description='Update speaker-level i-vectors of a single speaker (or a set of speakers) with a new set of i-vectors.')

    parser.add_argument('--weight', default=2)

    parser.add_argument('spk_ivectors_ark')
    parser.add_argument('spk_num_utts')

    parser.add_argument('new_ivectors_ark')
    parser.add_argument('new_num_utts')

    parser.add_argument('out_ivectors_ark')
    parser.add_argument('out_num_utts')

    args = parser.parse_args()
    
    spk_ivectors = {}
    with kaldiio.ReadHelper('ark:%s' % args.spk_ivectors_ark) as reader:
        for key, numpy_array in reader:
            spk_ivectors[key] = numpy_array

    spk_num_utts = {}
    for l in open(args.spk_num_utts):
      key, value = l.split()
      spk_num_utts[key] = int(value)

    new_ivectors = {}
    with kaldiio.ReadHelper('ark:%s' % args.new_ivectors_ark) as reader:
        for key, numpy_array in reader:
            new_ivectors[key] = numpy_array

    new_num_utts = {}
    for l in open(args.new_num_utts):
      key, value = l.split()
      new_num_utts[key] = int(value)
            
    for spk, ivector in new_ivectors.items():
        if spk not in spk_ivectors:
            spk_ivectors[spk] = new_ivectors[spk]
            spk_num_utts[spk] = new_num_utts[spk]
        else:
            spk_ivectors[spk] = (spk_ivectors[spk] * spk_num_utts[spk] + new_ivectors[spk] * new_num_utts[spk] * args.weight) / (spk_num_utts[spk] + new_num_utts[spk] * args.weight)
            spk_num_utts[spk] = spk_num_utts[spk] + new_num_utts[spk]
            norm = np.linalg.norm(spk_ivectors[spk])
            ratio = norm / np.sqrt(spk_ivectors[spk].shape[0])
            spk_ivectors[spk] /= ratio

    
    with kaldiio.WriteHelper('ark:%s' % args.out_ivectors_ark) as writer:
        for key, value in spk_ivectors.items():
            writer(key, value)

    with open(args.out_num_utts, "wb") as f:
        for key, value in spk_num_utts.items():
            f.write(("%s %d\n" % (key,value)).encode())



