#!/bin/bash

. ./cmd.sh
[ -f path.sh ] && . ./path.sh

num_threads=8
similiarity_threshold=5
cleanup=true

set -e

echo $* > /dev/stderr

. utils/parse_options.sh

dir=$1

function finish {
  rm -rf "$dir"
}
$cleanup && trap finish EXIT

mkdir -p $dir/test
/bin/ls $dir/audio/*.* | sort | awk '{printf("%04d ffmpeg -i %s  -f sox - | sox -t sox - -c 1 -b 16 -t wav - rate -v 16k |\n", NR, $1)}' > $dir/test/wav.scp 

cat $dir/test/wav.scp | awk '{print $1, "test"}' | sort > $dir/test/utt2spk
utils/utt2spk_to_spk2utt.pl $dir/test/utt2spk > $dir/test/spk2utt


steps/make_mfcc.sh --mfcc-config extractor/conf/mfcc_sid.conf --nj 1 $dir/test > /dev/stderr
steps/compute_cmvn_stats.sh $dir/test > /dev/stderr
sid/compute_vad_decision.sh --vad-config extractor/conf/vad.conf --nj 1 $dir/test > /dev/stderr

sid/extract_ivectors.sh --nj 1 --num-threads $num_threads  extractor/extractor_2048 $dir/test $dir/test_ivectors > /dev/stderr

# cross-product between trained speakers and test speaker
join -j 2 \
        <(copy-vector ark:speaker_data/name_ivector.ark ark,t:- | cut -d " " -f 1 | sort ) \
        <(cut -d " " -f 1 $dir/test_ivectors/spk_ivector.scp | sort ) > $dir/trials
  
ivector-plda-scoring --normalize-length=true \
    "ivector-copy-plda --smoothing=0.3 extractor/lda_plda - |" \
    "ark:ivector-subtract-global-mean ark:speaker_data/name_ivector.ark ark:- | transform-vec extractor/transform.mat ark:- ark:- | ivector-normalize-length ark:- ark:- |" \
    "ark:ivector-subtract-global-mean extractor/mean.vec scp:$dir/test_ivectors/spk_ivector.scp ark:- | transform-vec extractor/transform.mat ark:- ark:- | ivector-normalize-length ark:- ark:- |" \
    $dir/trials $dir/plda_lda.scores > /dev/stderr

sort -k3,3gr $dir/plda_lda.scores | head -1 | awk '{if ($3>'$similiarity_threshold') {print $1}}'
