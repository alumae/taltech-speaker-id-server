#!/usr/bin/env python3

import logging
import os
import uuid
import json
import datetime
import time
import shlex
import subprocess

import tornado
import tornado.httpserver
import tornado.httputil
import tornado.ioloop
import tornado.web
import tornado.process
from tornado.options import options


class PoiIdAlreadyExistsException(Exception):
   """Raised when such POI ID already exists"""
   pass

class NoSuchPoiException(Exception):
   """Raised when such POI ID is not found"""
   pass


class Application(tornado.web.Application):

    def __init__(self):
        self.ROOT = os.path.dirname(os.path.abspath(__file__))
        path = lambda root,*a: os.path.join(root, *a)
        settings = dict(
            cookie_secret="43oETzKXQAGaYdkL5gEmGeJJFuYh7EQnp2XdTP1o/Vo=",
            static_path=path(self.ROOT, 'static'),
            xsrf_cookies=False,
            autoescape=None,
            serve_traceback=True,
            debug=True
        )

        handlers = [
            tornado.web.url(r"/v1/pois", PoisHandler),
            tornado.web.url(r"/v1/pois/(.+)", PoisHandler),
            (r"/v1/enroll-poi/(.+)", EnrollHandler),
            (r"/v1/identify", IdentifyHandler)
        ]

        tornado.web.Application.__init__(self, handlers, **settings)
        self.running_requests = dict()

    def get_poi_file(self):
        return "%s/pois.json" % "speaker_data"

    def add_poi(self, id, props):
        json_file = self.get_poi_file()
        if os.path.exists(json_file):
            pois = json.load(open(json_file))
        else:
            pois = {}
        if id in pois:
            raise PoiIdAlreadyExistsException
        else:
            ts = time.time()
            pois[id] = props
            pois[id]["creationTime"] = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')
        json.dump(pois, open(json_file, "w"), sort_keys=True, indent=4)

    def get_pois(self):
        json_file = self.get_poi_file()
        if os.path.exists(json_file):
            pois = json.load(open(json_file))
        else:
            pois = {}
        return pois

    async def delete_poi(self, id):
        json_file = self.get_poi_file()
        if os.path.exists(json_file):
            pois = json.load(open(json_file))
        else:
            pois = {}
        if id in pois:
            del pois[id]
            cmd = "local/delete_speaker.sh %s" % id
            tornado.process.Subprocess.initialize()
            logging.info("Running command: %s" % cmd)
            proc = tornado.process.Subprocess(shlex.split(cmd), stdout=tornado.process.Subprocess.STREAM, stderr=tornado.process.Subprocess.STREAM, cwd=self.ROOT, shell=False)
            output = (await proc.stdout.read_until_close()).decode("utf-8") 
            err = (await proc.stderr.read_until_close()).decode("utf-8") 
            ret = await proc.wait_for_exit(raise_error=False)
            if ret != 0:
                raise Exception("Deletion failed. Log:\n%s" % err)

            json.dump(pois, open(json_file, "w"), sort_keys=True, indent=4)
        else:
            raise NoSuchPoiException

    async def modify_poi(self, id, props):
        json_file = self.get_poi_file()
        if os.path.exists(json_file):
            pois = json.load(open(json_file))
        else:
            pois = {}
        if id in pois:
            old_props = pois[id]
            del pois[id]
            pois[props["poi-id"]] = old_props
            pois[props["poi-id"]].update(props)
            if id != props["poi-id"]:
                cmd = "local/rename_speaker.sh %s %s" % (id, props["poi-id"])
                tornado.process.Subprocess.initialize()
                logging.info("Running command: %s" % cmd)
                proc = tornado.process.Subprocess(shlex.split(cmd), stdout=tornado.process.Subprocess.STREAM, stderr=tornado.process.Subprocess.STREAM, cwd=self.ROOT, shell=False)
                output = (await proc.stdout.read_until_close()).decode("utf-8") 
                err = (await proc.stderr.read_until_close()).decode("utf-8") 
                ret = await proc.wait_for_exit(raise_error=False)
                if ret != 0:
                    raise Exception("Rename failed. Log:\n%s" % err)
            json.dump(pois, open(json_file, "w"), sort_keys=True, indent=4)
        else:
            raise NoSuchPoiException

    async def identify_poi(self, dir):
        cmd = "local/identify_speaker.sh %s" % dir
        tornado.process.Subprocess.initialize()
        logging.info("Running command: %s" % cmd)
        proc = tornado.process.Subprocess(shlex.split(cmd), stdout=tornado.process.Subprocess.STREAM, stderr=tornado.process.Subprocess.STREAM, cwd=self.ROOT, shell=False)
        output = (await proc.stdout.read_until_close()).decode("utf-8") 
        err = (await proc.stderr.read_until_close()).decode("utf-8") 
        ret = await proc.wait_for_exit(raise_error=False)
        if ret != 0:
            raise Exception("Identification failed. Log:\n%s" % err)
        poi_id = output.strip()
        if len(poi_id) > 0:
            pois = self.get_pois()
            if poi_id in pois:
                return pois[poi_id]
            else:
                raise Exception("Idenfied speaker with ID %s but such POI id is not found" % poi_id)
        else:
            return None

    async def enroll_poi(self, dir, poi_id):
        pois = self.get_pois()
        if not poi_id in pois:
            raise NoSuchPoiException
        cmd = "local/enroll_speaker.sh %s %s" % (dir, poi_id)
        tornado.process.Subprocess.initialize()
        logging.info("Running command: %s" % cmd)
        proc = tornado.process.Subprocess(shlex.split(cmd), stdout=tornado.process.Subprocess.STREAM, stderr=tornado.process.Subprocess.STREAM, cwd=self.ROOT, shell=False)
        output = (await proc.stdout.read_until_close()).decode("utf-8") 
        err = (await proc.stderr.read_until_close()).decode("utf-8") 
        ret = await proc.wait_for_exit(raise_error=False)
        if ret != 0:
            raise Exception("Enrollment failed. Log:\n%s" % err)



class PoisHandler(tornado.web.RequestHandler):

    def initialize(self):
        pass

    def set_default_headers(self):
        self.set_header("Access-Control-Allow-Origin", "*")
        self.set_header("Access-Control-Allow-Headers", "x-requested-with")
        self.set_header('Access-Control-Allow-Methods', 'POST, GET, DELETE, OPTIONS')        
        self.set_header("Access-Control-Allow-Headers", "access-control-allow-origin,authorization,content-type") 

    def compute_etag(self):
        return None

    def options(self):
        # no body
        self.set_status(204)
        self.finish()

    async def post(self, slug=None):
        props = json.loads(self.request.body.decode('utf-8'))
        poi_id = props["poi-id"]        

        if slug:
            try:
                await self.application.modify_poi(slug, props)
                self.set_status(201, reason="Modified POI with ID %s" % slug)
                self.set_header("Location", "v1/pois/%s" % props["poi-id"])
            except NoSuchPoiException:
                self.send_error(status_code=404, reason="Speaker with ID %s does not exists" % slug)
        else:
            try:
                self.application.add_poi(poi_id, props)
                self.set_status(201, reason="Created POI with ID %s" % poi_id)
                self.set_header("Location", "v1/pois/%s" % poi_id)
            except PoiIdAlreadyExistsException:
                self.send_error(status_code=409, reason="Speaker with ID %s already exists" % poi_id)

    def get(self, slug=None):
        pois = self.application.get_pois()    
        if slug:
            if slug in pois:
                self.write(json.dumps(pois[slug], sort_keys=True, indent=4))        
            else:
                self.send_error(status_code=404, reason="Speaker with ID %s not found" % slug)
        else:
            self.set_header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
            result = {"pois": pois}
            self.write(json.dumps(result, sort_keys=True, indent=4))

    async def delete(self, slug=None):
        if slug:
            try:
                await self.application.delete_poi(slug)    
                self.set_status(204, reason="Deleted POI with ID %s" % slug)
            except NoSuchPoiException:
                self.send_error(status_code=404, reason="Speaker with ID %s not found" % slug)


class EnrollHandler(tornado.web.RequestHandler):

    def initialize(self):
        self.request_id = str(uuid.uuid4())

    async def put(self, slug):
        poi_id = slug
        content_type = self.request.headers['Content-Type']
        if content_type == "audio/mpeg":
            extension = "mpg"
        elif content_type in ["audio/wav", "audio/x-wav"]:
            extension = "wav"
        else:
            self.send_error(status_code=400, reason="Content-Type should be 'audio/mpg' or 'audio/wav'") 
        file_body = self.request.body
        dir = "%s/%s" % (tornado.options.options.work_dir, self.request_id)
        os.makedirs(dir + "/audio")
        filename = "%s/audio/0001.%s" % (dir, extension)
        with open(filename, "wb") as f:
            f.write(file_body)
        try:
            await self.application.enroll_poi(dir, poi_id)
        except NoSuchPoiException:
            self.send_error(status_code=404, reason="Speaker with ID %s not found" % slug)


class IdentifyHandler(tornado.web.RequestHandler):

    def initialize(self):
        self.request_id = str(uuid.uuid4())

    async def post(self):
        upload_type = self.get_argument("uploadType")
        if upload_type == "media":
            content_type = self.request.headers['Content-Type']
            if content_type == "audio/mpeg":
                extension = "mpg"
            elif content_type in ["audio/wav", "audio/x-wav"]:
                extension = "wav"
            else:
               self.send_error(status_code=400, reason="Content-Type should be 'audio/mpg' or 'audio/wav'") 
            file_body = self.request.body
            dir = "%s/%s" % (tornado.options.options.work_dir, self.request_id)
            os.makedirs(dir + "/audio")
            filename = "%s/audio/0001.%s" % (dir, extension)
            with open(filename, "wb") as f:
                f.write(file_body)

            result = await self.application.identify_poi(dir)
            if result:
                self.write(json.dumps(result, sort_keys=True, indent=4))
            else:
                self.send_error(status_code=204, reason="Speaker not identified")

        elif upload_type == "multipart":
            content_type = self.request.headers.get("Content-Type")
            # expected: Content-Type: Multipart/Related; boundary=example-1
            if content_type.lower().startswith("multipart/related"):
                boundary = content_type[content_type.find("boundary")+9:]
                if len(boundary) > 0:
                    if boundary.startswith('"') and boundary.endswith('"'):
                        boundary = boundary[1:-1]
                    final_boundary_index = self.request.body.rfind(("--" + boundary + "--").encode("ASCII"))
                    if final_boundary_index == -1:
                        self.send_error(status_code=400, reason="Invalid multipart/related: no final boundary")
                        return
                    parts = self.request.body[:final_boundary_index].split(b"--" + boundary.encode("ASCII") + b"\r\n")                        
                    dir = "%s/%s" % (tornado.options.options.work_dir, self.request_id)
                    os.makedirs(dir + "/audio")

                    for i, part in enumerate(parts):
                        if not part:
                            continue
                        eoh = part.find(b"\r\n\r\n")
                        if eoh == -1:
                            self.send_error(status_code=400, reason="multipart/related part missing headers")
                            return
                        headers = tornado.httputil.HTTPHeaders.parse(part[:eoh].decode("utf-8"))
                        content_type = headers['Content-Type']
                        if content_type == "audio/mpeg":
                            extension = "mpg"
                        elif content_type in ["audio/wav", "audio/x-wav"]:
                            extension = "wav"
                        else:
                            self.send_error(status_code=400, reason="Content-Type should be 'audio/mpg' or 'audio/wav'") 
                        file_body = part[eoh + 4 : -2]
                        
                        filename = "%s/audio/%04d.%s" % (dir, i, extension)
                        with open(filename, "wb") as f:
                            f.write(file_body)
                    result = await self.application.identify_poi(dir)
                    if result:
                        self.write(json.dumps(result, sort_keys=True, indent=4))
                    else:
                        self.set_status(status_code=204, reason="Speaker not identified")
                else:
                    self.set_status(status_code=400, reason="Content-type is not in the form of multipart/related; boundary=example-1")                    
            else:
                self.set_status(status_code=400, reason="Content-type is not in the form of multipart/related; boundary=example-1")                

        else:
            self.set_status(status_code=400, reason="Request argument 'uploadType should be 'media' or 'multipart'")

            
def main():
    tornado.log.enable_pretty_logging()

    logging.debug('Starting up server')

    tornado.options.define("port", default=8876, help="run on the given port", type=int)
    tornado.options.define("work_dir", default="work", help="Directory for storing temporary files")
    tornado.options.define("config", type=str, help="path to config file", callback=lambda path: tornado.options.parse_config_file(path, final=True))
    tornado.options.parse_command_line()
    tornado.options.options.work_dir = os.path.expanduser(tornado.options.options.work_dir)
    app = Application()
    logging.info('Listening on port %d' % tornado.options.options.port)
    app.listen(tornado.options.options.port, max_buffer_size=4*1024*1024*1024) # max upload size is 4GB
    tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    main()

