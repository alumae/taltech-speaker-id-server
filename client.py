import argparse

import requests

from urllib3.filepost import encode_multipart_formdata, choose_boundary
from urllib3.fields import RequestField

from email.mime.multipart import MIMEMultipart
from email.mime.nonmultipart import MIMENonMultipart
from email.mime.audio import MIMEAudio
import logging

#import http.client as http_client
#http_client.HTTPConnection.debuglevel = 1

def encode_multipart_related(fields, boundary=None):
    if boundary is None:
        boundary = choose_boundary()

    body, _ = encode_multipart_formdata(fields, boundary)
    content_type = str('multipart/related; boundary=%s' % boundary)

    return body, content_type

def encode_media_related(audio_files):
    rfs = []
    for f in audio_files:
        
        rf = RequestField(
            name='placeholder2',
            data=open(f, 'rb').read(),
            headers={'Content-Type': "audio/wav"},
        )
        rfs.append(rf)
    return encode_multipart_related(rfs)

if __name__ == "__main__":

    
    parser = argparse.ArgumentParser(description='Send audio files to the server')

    parser.add_argument('--url', default="http://localhost:8888/v1/identify?uploadType=multipart")
    parser.add_argument('file', nargs='+')

    args = parser.parse_args()
    

body, content_type = encode_media_related(args.file)

# You must initialize logging, otherwise you'll not see debug output.
logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)
#requests_log = logging.getLogger("requests.packages.urllib3")
#requests_log.setLevel(logging.INFO)
#requests_log.propagate = True

r = requests.post(args.url, data=body, headers={"Content-Type": content_type})

print(r.content)
